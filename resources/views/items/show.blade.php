@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
            <div class="container">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>Price</td>
                        <td>Avatar</td>
                        <td>Created</td>
                        <td>Updated</td>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->price}}</td>
                            <td><img src="{{$item->avatar}}" alt="{{$item->avatar}}" width="30%"></td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->updated_at}}</td>

                        </tr>
                    </tbody>
                </table>
            </div>
@endsection

