@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="/items/create" class="btn btn-success">Create</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Avatar</td>
                <td>Edit</td>
                <td>Show</td>
                <td>Delete</td>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td><img src="{{$item->avatar}}" alt="{{$item->avatar}}" width="30%"></td>

                    <td><a href="{{action('ItemController@edit',$item->id)}}" class="btn btn-warning">Edit</a></td>
                    <td><a href="{{action('ItemController@show',$item->id)}}" class="btn btn-primary">Show</a></td>
                    <td>
                        <form action="{{action('ItemController@destroy', $item->id)}}" method="post">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
@endsection

