@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <div class="row">
            <form method="post" action="{{action('ItemController@update', $id)}}"  enctype="multipart/form-data">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PATCH">
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <label for="title">Ticket name:</label>
                    <input type="text" class="form-control" name="name" value={{$item->name}} />
                </div>
                <div class="form-group">
                    <label for="description">Ticket price:</label>
                    <input type="text" class="form-control" name="price" value={{$item->price}} />
                </div>
                <div class="form-group">
                    <label for="price">Upload an image:</label>
                    <input type="file" name="avatar" value="{{$item->avatar}}">

                    <img src="{{$item->avatar}}" alt="pic"width="30%">

                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection