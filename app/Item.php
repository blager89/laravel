<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model

{

    protected $fillable = ['name', 'price', 'avatar'];

    public function updateItem($data)
    {
        $item = $this->find($data['id']);
        $item->name = $data['name'];
        $item->price = $data['price'];
        if ($data['avatar'] !== false) {

            $item->avatar = $data['avatar'];
        }

        $item->updated_at =  date("Y-m-d H:i:s");

        $item->save();
        return 1;
    }

    public function uploadImg($request,$items = null,$column_name = null)
        {

            if($request->isMethod('post')){
                if($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                    $file->move(public_path() . '/uploads',$file->getClientOriginalName());

                    if ($items !== null || $column_name !== null) {

                        $items->$column_name = '/uploads/' .$file->getClientOriginalName();
                    }

                }
            }
        }

    public function updateImg($request,$items = null,$column_name = null)
    {
        if($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $file->move(public_path() . '/uploads',$file->getClientOriginalName());

            if ($items !== null || $column_name !== null) {

                $items->$column_name = '/uploads/' .$file->getClientOriginalName();
            }


        }
        return '/uploads/' .$file->getClientOriginalName();

    }







}