<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests\ItemRequest;

/**

 * Store a newly created resource in storage.

 *

 * @param \Illuminate\Http\Request $request

 * @return \Illuminate\Http\Response

 */

date_default_timezone_set('Europe/Kiev');

class ItemController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('items.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

            return view('items.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)

    {
//        $items = $request->all();
        $items = new Item($request->all());
        $column_name = 'avatar';
        $items->uploadImg($request,$items,$column_name);

        $items->save();

        return redirect('/items');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::find($id);
        return view('items.show',compact('item','id'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return view('items.edit', compact('item', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = new Item();
        $data = $this->validate($request, [
            'name'=>'required',
            'price'=> 'required'
        ]);
        $data['id'] = $id;
        if ($request->hasFile('avatar')) {
            $data['avatar'] = $ticket->updateImg($request,$ticket,'avatar');
        }else{
            $data['avatar'] = false;
        }



        $ticket->updateItem($data);
        return redirect('/items')->with('success', 'New support ticket has been updated!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Item::find($id);
        $ticket->delete();

        return redirect('/items')->with('success', 'Ticket has been deleted!!');
    }







}
